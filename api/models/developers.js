// SCI Developers Schema

var mongoose = require('mongoose');
var Schema	 = mongoose.Schema;

var ActivityLog = new Schema({
	ip: String,
	date: { type: Date, default: Date.now }
})

var DeveloperSchema = new Schema({
	name: String,
	email: String,
	password: String,
	key: String,
	active: { type: Boolean, default: true },
	internal: { type: Boolean, default: false},
	log: [ActivityLog],
	domains: { type: Array, default: []}
});

// Method for validating key
DeveloperSchema.statics.isKeyValid = function isKeyValid (devKey, cb) {
	this.findOne({key: devKey}, cb);
}

module.exports = mongoose.model('developers', DeveloperSchema);