var mongoose = require('mongoose');
var Schema	 = mongoose.Schema;

var ShopSchema = new Schema({
	approved: {type: Boolean, default: false},
	name: String,
	phone: String,
	customUrl: String,
	background: {type: String, default: "static/wallpaper_tattoo_blur2.png"},
	glance: {
		walkIns: {type: Boolean, default: false},
		knownFor: String
	},
	license: {
		id: {type: String, index: { unique: true }},
		state: String,
		history: [LicenseSchema]
	},	
	address: {
		address1: String,
		address2: String,
		city: String,
		state: String,
		zip: String
	},
	hours: {
		sunday: {
			isOpen: {type: Boolean, default: false},
			open: String,
			close: String
		},
		monday: {
			isOpen: {type: Boolean, default: false},
			open: String,
			close: String
		},
		tuesday: {
			isOpen: {type: Boolean, default: false},
			open: String,
			close: String
		},
		wednesday: {
			isOpen: {type: Boolean, default: false},
			open: String,
			close: String
		},
		thursday: {
			isOpen: {type: Boolean, default: false},
			open: String,
			close: String
		},
		friday: {
			isOpen: {type: Boolean, default: false},
			open: String,
			close: String
		},
		saturday: {
			isOpen: {type: Boolean, default: false},
			open: String,
			close: String
		}
	},
	adminList: [{ type: Schema.Types.ObjectId, ref: 'users' }],
	artists: [{ type: Schema.Types.ObjectId, ref: 'users' }],
	reviews: [ReviewSchema]
});

var LicenseSchema = new Schema({
	filePath: String,
	start: Date,
	expiration: Date
})

var ReviewSchema = new Schema({
	author: { 
		id: Schema.Types.ObjectId,
		fName: String,
		lName: String
	},
	date: { type: Date, default: Date.now },
	text: String,
	professionalism: Number,
	cleanliness: Number,
	quality: Number,
	cost: Number,
	overall: Number
})



module.exports = mongoose.model('shops', ShopSchema);