// SCI Gallery Schema

var mongoose = require('mongoose');
var Schema	 = mongoose.Schema;

var commentSchema = new Schema({
	text: String,
	author: { type: Schema.Types.ObjectId, ref: 'users' },
	date: { type: Date, default: Date.now }
});

var PhotoSchema = new Schema({
	title: String,
	path: String,
	caption: String,
	tags: [],
	created: { type: Date, default: Date.now },
	artist: { type: Schema.Types.ObjectId, ref: 'users' },
	comments: [commentSchema]
});

module.exports = mongoose.model('gallery', PhotoSchema);
