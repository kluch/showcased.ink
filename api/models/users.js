// SCI Users Schema

var mongoose   = require('mongoose');
var Schema	   = mongoose.Schema;


var UserSchema = new Schema({
	email: String,
	password: String,
	fName: String,
	lName: String,
	birthday: Date,
	facebook: {
		id: String,
		token: String
	},
	isStaff: {type: Boolean, default: false},
	showMature: {type: Boolean, default: false},
	artist: {
		isArtist: {type: Boolean, default: false},
		nickname: String,
		url: String,
		photo: {type: String, default: "static/user.png"}, // Image path
		bio: String,
		address: {
			address1: String,
			address2: String,
			city: String,
			state: String,
			zip: String
		},
		socialMedia: {
			instagram: String,
			facebook: String,
			twitter: String
		},
		reviews: [ReviewSchema]
	},
	auth: {
		token: String,
		expiration: Date
	},
	meta: {
		commentCount: {type: Number, default: 0}
	}
});

var ReviewSchema = new Schema({
	author: { 
		id: Schema.Types.ObjectId,
		fName: String,
		lName: String
	},
	date: { type: Date, default: Date.now },
	text: String,
	professionalism: Number,
	cleanliness: Number,
	quality: Number,
	cost: Number,
	overall: Number
})

UserSchema.statics.userExists = function (user, cb) {
	this.findOne({email: user}, cb);
}

UserSchema.statics.isUrlTaken = function (url, cb) {
	this.findOne({"artist.url": url}, cb);
};

UserSchema.statics.isTokenValid = function(token, cb) {
	this.findOne({"auth.token": token}, cb);
}

// Don't pass password to JSON object.
UserSchema.methods.toJSON = function() {
  var obj = this.toObject();
  delete obj.password
  return obj
}

module.exports = mongoose.model('users', UserSchema);
	
