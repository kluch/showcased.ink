// Showcased.Ink Web Services
// Created Jan 13, 2015

var express 	= require('express');
var cors 		= require('cors'); // Allow Cross-Domain requests.
var app 		= express();
var bodyParser	= require('body-parser');
var mongoose	= require('mongoose');
var colors      = require('colors');
var passport    = require('passport');
var expressSession = require('express-session');
var MongoStore = require('connect-mongo')(expressSession);
var dbBuilder   = require('./dbBuilder');

// Import passport config.
require('./passport')(app);

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.json());

// Allows for cross-domain requests.
app.use(cors()); 

// Database connection
dbBuilder.connectDb();

// Persist sessions
app.use(expressSession({
	secret: 'BeMySamurai343',
	resave: false,
	saveUninitialized: false,
	 store: new MongoStore({
      mongooseConnection: mongoose.connection
    })
}));

// Facebook authentication
app.use(passport.initialize());
app.use(passport.session());

var port = process.env.PORT || 8080;

var routes = require('./router')(app);	

// Catch Mongoose connection errors.
mongoose.connection.on("error", function(err) {
  console.error("Could not connect to mongo server:".red);
  return console.log(err.message.red);
});

mongoose.connection.on("open", function() {
  if (this.name == "sci-dev")
  	console.log("Connected to " + "DEVELOPMENT".yellow + " database " + "(" + this.host + ":" + this.port + ").");
  else
  	console.log("Connected to " + "BETA".cyan + " database " + "(" + this.host + ":" + this.port + ").");
});


// Start this puppy up!
app.listen(port);

console.log('\nShowcased.Ink server is running on port ' + port + ' (01.13.2015)');