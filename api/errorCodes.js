// Server Error codes
// 0
module.exports = {
	BAD_REQUEST		: 400,
    UNAUTHORIZED	: 401,
    GENERAL			: 500
};