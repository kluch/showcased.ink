var passport         = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var LocalStrategy    = require('passport-local').Strategy;
var Users            = require('./models/users');
var bcrypt           = require('bcrypt');

module.exports = function (app) {

	passport.serializeUser(function(user, done) {   
           done(null, user._id);
  });

  passport.deserializeUser(function(id, done) {
        Users.findById(id, function(err, user) {
            done(err, user);
        });
    });

  // Configure local authentication
  passport.use(new LocalStrategy({
    usernameField: 'email'
  },
    function (username, password, done) {

      Users.findOne({'email': username}, function (err, user) {
        if (err) { 
          return done(err) 
        };
    
        if(!user) {
          return done(null, false, { message: "Incorrect credentials." });
        }

        if(!bcrypt.compareSync(password, user.password)) {
          return done(null, false, { message: "Incorrect credentials." });
        }

        return done(null, user);

      })
    }))

  // Configure Facebook authentication
  passport.use(new FacebookStrategy({
      clientID: 784558704933296,
      clientSecret: "57576760ddb4a78ed0ee044e4f21fdb7",
      callbackURL: "http://localhost:8080/accounts/login/fb"
    },
    function(accessToken, refreshToken, profile, done) {
      Users.findOne({'facebook.id': profile.id}, function (err, user) {

        if (err) { 
          return done(err); 
        }

        if (user) {
          return done(null, user);
        } else {
          // User doesn't exist - create new user.
           var newUser = Users();
           newUser.facebook.id = profile.id;
           newUser.facebook.token = accessToken;
           newUser.fName = profile.name.givenName;
           newUser.lName = profile.name.familyName;

           newUser.save(function (err) {
            if(err) 
              throw err;

            // Return the new user
            return done(null, newUser);
           })
        }
      })
    }
  ));
};