var apiTools = require('./apiTools');
var multer = require('multer');
var fs = require('fs');
var Users = require('../models/users');

module.exports = function (app) {

	// Set default content type for requests
	app.use(function (req, res, next) {
  		res.contentType('application/json');

  		// Set API in private mode - Only whitelisted IPs can access IP.
  		// if(apiTools.privateMode(req, res)) {
    //     next();
    //   } else {
    //     res.json({error: "Public API is not available."})
    //   }
    next();
});


// Form-data/multipart for file upload. 
app.use(multer({
    dest: './cdn/gallery/',
    rename: function (fieldname, filename) {
      return filename.replace(/ /g, "_") + "_" + Date.now();
    },
    onFileSizeLimit: function (file) {
      fs.unlink('./cdn/gallery/' + file.path)
    },
    limits: {
      fileSize: apiTools.uploadLimit * 1000000 // Convert megabytes to bytes
    }
  }));

	// Define API routes
  app.use('/demo', require('./routes/demo'));
  app.use('/developers', require('./routes/developers'));
  app.use('/accounts', require('./routes/accounts'));
  app.use('/shops', require('./routes/shops'));
  app.use('/artists', require('./routes/artists'));
  app.use('/gallery', require('./routes/gallery'));
  app.use('/profiles', require('./routes/profile'));
  app.use('/search', require('./routes/search'));
  app.use('/photo', require('./routes/photo'));
  app.use('/staff', require('./routes/staff'));
};