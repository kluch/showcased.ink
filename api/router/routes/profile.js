// Showcased.Ink Web Services
// File Description: Profile management methods
// Created Feb 16, 2015

var express 	= require('express');	
var router 		= express.Router();
var Users 		= require('../../models/users');
var Shops 		= require('../../models/shops');
var errorCodes  = require('../errorCodes');
var mongoose 	= require('mongoose');


router.post('/getInformation', function (req, res) {

	if(!req.body.user_id) { return res.json({error: "user_id is required."}) };

	Users.findOne({"_id": req.body.user_id}, function (err, data) {
		if(err) {
			return res.json({error: "An error occurred."});
		} else {
			return res.json({
				fName: data.fName,
				lName: data.lName,
				artist: {
					isArtist: data.artist.isArtist,
					url: data.artist.url
				},
				meta: data.meta
			});
		}
	});
});

// Check if the profile belongs to the logged in user.
router.post('/isMe', isTokenExpired, function (req, res) {
	if(!req.body.user_id) { return res.json({error: "user_id is required."}) };
	
	Users.findOne({"auth.token": req.body.token}, function(err, data) {
		if(err) { return res.json({error: "user_id is required."}) };

		if(data._id == req.body.user_id) {
			res.json({message: true})
		} else {
			res.json({message: false})
		}
	});
});

router.get('/:id/allReviews', function (req, res) {
	Shops.find({'reviews.author.id': mongoose.Types.ObjectId(req.param('id'))}, function (err, data) {
		return res.json(data);
	});
})


function isTokenExpired(req, res, next) {
	if(!req.body.token) { return res.json({error: "token required."}) }

	Users.isTokenValid(req.body.token, function(err, data) {
		if(data) {
			next();
		} else {
			return res.json({message: "Invalid token."});
		}
	});
}

module.exports = router;