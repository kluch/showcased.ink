// Showcased.Ink Web Services
// File Description: Shop management methods
// Created Feb 9, 2014

var express 	= require('express');
var router 		= express.Router();
var Shop 		= require('../../models/shops');
var Users 		= require('../../models/users');
var errorCodes  = require('../errorCodes');

router.post('/license-upload', function (req, res) {
	if(req.files.file) {
		//return res.json({message: "Success!"});
		return res.json(req.files.file);
	}
});

// Create Shop
router.post('/create', isTokenExpired, function (req, res) {
	// Required Fields
		if(!req.body.newShop.name) {
			res.json({"error": "Shop name is required."});
			return;
		}

		if(!req.body.newShop.phone) {
			res.json({"error": "Phone number is required."});
			return;
		}

		if(!req.body.newShop.license_id) {
			res.json({"error": "License ID is required."});
			return;
		}

		if(!req.body.newShop.license_state) {
			res.json({"error": "State of license is required."});
			return;
		}

		if(!req.body.newShop.license_exp) {
			res.json({"error": "Expiration of license is required."});
			return;
		}

		if(!req.body.file) { 
			res.json({error: "license file is required."});
			return;
		}

		if(!req.body.newShop.addr) {
			res.json({"error": "Address line 1 is required."});
			return;
		}

		if(!req.body.newShop.city) {
			res.json({"error": "City is required."});
			return;
		}

		if(!req.body.newShop.state) {
			res.json({"error": "State code is required."});
			return;
		}

		if(!req.body.newShop.zip) {
			res.json({"error": "Zip code is required."});
			return;
		}

		if(!req.body.newShop.url) {
			res.json({"error": "URL slug is required."});
			return;
		}

	Shop.findOne({'customUrl': req.body.newShop.url}, function (err, result) {
		if(result) {
			console.log(result);
			return res.json({"error": "Custom URL is taken."});
		}
	});	

	// Check if shop exists by license ID.
	Shop.findOne({'license.id': req.body.newShop.license_id}, function (err, result) {
		if(result) {
			return res.json({"error": "Shop with license ID already exists."});
		} else {
			// Assign parameters to new shop.
			var newShop = new Shop();

			newShop.name 				= req.body.newShop.name;
			newShop.phone 				= req.body.newShop.phone;

			// License information
			newShop.license.id 			= req.body.newShop.license_id;
			newShop.license.state 		= req.body.newShop.state;

			newShop.license.history.push({
				filePath: req.body.file.path,
				start: Date.now(),
				expiration: req.body.newShop.license_exp
			});

			// Address information
			newShop.address.address1	= req.body.newShop.addr;
			newShop.address.address2	= req.body.newShop.addr2;
			newShop.address.city    	= req.body.newShop.city;
			newShop.address.state   	= req.body.newShop.state;
			newShop.address.zip   		= req.body.newShop.zip;

			newShop.customUrl  			= req.body.newShop.url;

			// Add registering user to admin list.

			if(req.body.newShop.isArtist) {
				newShop.artists.push(req.body.user.id);
			}

			newShop.adminList.push(req.body.user.id);

			newShop.save(function (err, doc) {
				if(err) {
					return res.json({"error": "Error saving shop information. Please try again."});
				} else {
					return res.json({customUrl: doc.customUrl});
				}
			});
		}
	})
});

router.post('/:id/update', isTokenExpired, isAdmin, function (req, res) {

	var info = req.body.newData;

	Shop.findOne({"customUrl": req.param('id')}, function (err, data) {
		data.name = info.name;
		data.phone = info.phone;
		data.customUrl = info.customUrl;
		data.address.address1 = info.address.address1;
		data.address.address2 = info.address.address2;


		if(req.body.file) {
			data.background = req.body.file.name;
		};

		data.save(function (err, doc) {
			return res.json("Shop updated.");
		});
	});
})

// Return whether artist is a member of specified shop
router.post('/isartist', isTokenExpired, function (req, res) {
	if(!req.body.shop_id) { res.json({error: "shop_id is required."}); return; }

	if(!req.user.artist.isArtist) {
		res.json({isArtist: false});
	} else {
		Shops.count({_id: req.body.shop_id, adminList: req.user._id}, function (err, count) {
			if(count > 0) {
				res.json({isArtist: true});
			} else {
				res.json({isArtist: false});
			}
		});	
	}
});

// Return whether user is an admin of specified shop
router.post('/isadmin/:id', isTokenExpired, function (req, res) {
	Shop.findOne({"customUrl": req.param('id')}, function (err, data) {
		if(!data) {
			return res.json({"error": "Shop not found."});
		}

		for (admin in data.adminList) {
			if(data.adminList[admin].toString() == req.body.user.id.toString()) {
				return res.json(true);
			}
		}

		return res.json(false);
	});
});

// Return the full details of a shop.
router.get('/details/:id', function (req, res) {
	Shop.findOne({"customUrl": req.param('id')})
	.populate('artists')
	.populate('adminList').exec(function (err, data) {
		
		if(!data) {
			return res.json({"error": "No shops with id found."});
		}

		return res.json(data);
	
	});
});



// Add artist
router.post('/:id/add/artist', isTokenExpired, isAdmin, function (req, res) {

	if (!req.body.artistId) {
		return res.json({"error": "artistId required."});
	}

	Shop.findOne({"customUrl": req.param('id')}, function (err, data) {
		Users.findOne({"email": req.body.artistId}, function (err, userResults) {
			if(!userResults) {
				return res.json({error: "Artist not found."});
			}

			if(data.artists.indexOf(userResults._id) > -1) {
				return res.json({error: "Artist already in shop."});
			}

			data.artists.push(userResults._id);

			data.save(function (err, data) {
				return res.json("Artist saved.");
			});
		})
	});
});

// Remove artist
router.post('/:id/remove/artist', isTokenExpired, isAdmin, function (req, res) {
	if (!req.body.artistId) {
		return res.json({"error": "artistId required."});
	}

	Shop.findOne({"customUrl": req.param('id')}, function (err, data) {
		data.artists.pop(req.body.artistId);

		data.save(function (err, data) {
			return res.json("Artist removed.");
		});
	});
});

// Add admin
router.post('/:id/add/admin', isTokenExpired, isAdmin, function (req, res) {

	if (!req.body.adminId) {
		return res.json({"error": "adminId required."});
	}

	Shop.findOne({"customUrl": req.param('id')}, function (err, data) {
		Users.findOne({"email": req.body.adminId}, function (err, userResults) {
			if(!userResults) {
				return res.json({error: "User not found."});
			}

			if(data.adminList.indexOf(userResults._id) > -1) {
				return res.json({error: "User already admin."});
			}

			data.adminList.push(userResults._id);

			data.save(function (err, data) {
				return res.json("Admin saved.");
			});
		})
	});
});

// Remove admin
router.post('/:id/remove/admin', isTokenExpired, isAdmin, function (req, res) {
	if (!req.body.adminId) {
		return res.json({"error": "adminId required."});
	}

	Shop.findOne({"customUrl": req.param('id')}, function (err, data) {
		data.adminList.pop(req.body.adminId);

		data.save(function (err, data) {
			return res.json("admin removed.");
		});
	});
});

// Change hours
router.post('/:id/hours', isTokenExpired, isAdmin, function (req, res) {
	Shop.findOne({"customUrl": req.param('id')}, function (err, data) {

		data.hours = req.body.hours;

		data.save(function() {
			return res.json(data.hours);
		});
	});
});

// Change 'At a Glance' info
router.post('/:id/glance', isTokenExpired, isAdmin, function (req, res) {
	Shop.findOne({"customUrl": req.param('id')}, function (err, data) {

		data.glance = req.body.glance;

		data.save(function() {
			return res.json(data.glance);
		});
	});
});

// Add review
router.post('/:id/review', isTokenExpired, function (req, res) {
	Shop.findOne({"customUrl": req.param('id')}, function (err, data) {

		review = {
			author: {
				id: req.body.user.id,
				fName: req.body.user.fName,
				lName: req.body.user.lName
			},
			text: req.body.review.text,
			professionalism: req.body.review.pro,
			cleanliness: req.body.review.clean,
			quality: req.body.review.quality,
			cost: req.body.review.cost,
			overall: req.body.review.overall,
			date: Date.now()
		};

		data.reviews.push(review);

		data.save(function (err, data) {
			return res.json(data);
		})
	});
});

// ROUTING FUNCTIONS
function isTokenExpired(req, res, next) {
	if(!req.body.token) { return res.json({error: "token required."}) }

	Users.isTokenValid(req.body.token, function(err, data) {
		if(data) {
			Users.findOne({"auth.token": req.body.token}, function (err, data) {
				req.body.user = {id: data._id, fName: data.fName, lName: data.lName};
				next();
			});
		} else {
			return res.json({message: "Invalid token."});
		}
	});
}

function isAdmin(req, res, next) {
	Shop.findOne({"customUrl": req.param('id')}, function (err, data) {
		if(!data) {
			return res.json({"error": "Shop not found."});
		}

		for (admin in data.adminList) {
			if(data.adminList[admin].toString() == req.body.user.id.toString()) {
				return next();
			}
		}

			return res.json({"error": "User is not admin."});
	});
}

module.exports = router;