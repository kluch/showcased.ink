// Showcased.Ink Web Services
// File Description: Test methods.
// Created Jan 13, 2015

var express = require('express');
var router 	= express.Router();
var Developers 	= require('../../models/developers');
var errorCodes  = require('../errorCodes');

// GET /
router.get('/', function (req, res) {
    res.json({ message: 'Hooray! Great success!' })
});

// GET /validate - return whether given	 key is active and valid.
router.post('/validate', function(req, res) {
	if(!req.body.devKey) {
		res.send(errorCodes.BAD_REQUEST, "Missing 'devKey' parameter.")
	}

	Developers.isKeyValid(req.body.devKey, function(err, obj) {
		if (err) {
			res.send(errorCodes.GENERAL, "Server error occurred.")
		}
		
		if(obj == null) {
			res.send(errorCodes.UNAUTHORIZED, "Dev key not valid.")
		} else {
			res.json({name: obj.name});
		}
	})
})


// GET /ip - return request ip and port.
router.get('/ip', function (req, res) {
    res.json({ 
    	'remoteIp': req.connection.remoteAddress + ':' + req.connection.remotePort,
    	'remoteDomain': req.get('host')
     })
});

module.exports = router;