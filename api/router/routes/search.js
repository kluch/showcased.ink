// Showcased.Ink Web Services
// File Description: Search methods
// Created Apr 26, 2015

var express 	= require('express');	
var router 		= express.Router();
var Users 		= require('../../models/users');
var errorCodes  = require('../errorCodes');
var Gallery		= require('../../models/gallery');
var Shops		= require('../../models/shops');


router.get('/artists/:artist', function (req, res) {
	Users.find({'artist.isArtist': true}).or([
		{email: { $regex : new RegExp(req.param('artist'), "i") }}, 
		{fName: { $regex : new RegExp(req.param('artist'), "i") }},
		{lName: { $regex : new RegExp(req.param('artist'), "i") }},
		{'artist.nickname': { $regex : new RegExp(req.param('artist'), "i") }}
		]).exec(function (err, data) {
			return res.json(data);
		})
});

router.get('/tags/:tag', function (req, res) {
	Gallery.find ( { $or: [ 
		{'tags': { $regex : new RegExp("^"+ req.param('tag'), "i") }},
		{'title': { $regex : new RegExp(req.param('tag'), "i") }}
		]} )
	.populate('artist', 'fName lName artist.url')
	.exec(function (err, data) {
		return res.json(data);
	})
});

router.get('/shops/:shop', function (req, res) {
	Shops.find({name: new RegExp(req.param('shop'), "i")}).populate('artists', 'fName lName artist.url artist.photo').exec(function (err, data) {
		return res.json(data);
	})
});

module.exports = router;