// Showcased.Ink Web Services
// File Description: Shop management methods
// Created May 18, 2015

var express 	= require('express');
var router 		= express.Router();
var Shop 		= require('../../models/shops');
var Users 		= require('../../models/users');
var errorCodes  = require('../errorCodes');

//Get all shops that need approval
router.get('/unauthorizedshops', isTokenExpired, function(req, res){
	
	Shop.find({'approved': true}).exec(function (err, data) {
			return res.json(data);
		});
		
});

//Approve a single shop
router.post('/approveshop/:id', isTokenExpired, function(req, res){
	
	Shop.findOne({'_id': req.param('id')}, function (err, result) {
		
		if(result) {
			result.approved = true;
			result.save(function(err, doc){
				return res.json({
					message: "Shop Approved"
				});
			});	
		}
		else{
			return res.json({error: 'Shop ID not found.'});
		}
	});
	
});


// ROUTING FUNCTIONS
function isTokenExpired(req, res, next) {
	if(!req.body.token) { return res.json({error: "token required."}) }

	Users.isTokenValid(req.body.token, function(err, data) {
		if(data) {
			Users.findOne({"auth.token": req.body.token}, function (err, data) {
				req.body.user = {id: data._id, fName: data.fName, lName: data.lName};
				next();
			});
		} else {
			return res.json({message: "Invalid token."});
		}
	});
}

module.exports = router;