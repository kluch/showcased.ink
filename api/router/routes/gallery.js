var express 	= require('express');
var router 		= express.Router();
var Gallery     = require('../../models/gallery');
var Users     	= require('../../models/users');
var mongoose 	= require('mongoose');
var fs 			= require('fs');

// Gallery upload

router.post('/upload-file', function (req, res) {
	if(req.files.file) {
		//return res.json({message: "Success!"});
		return res.json(req.files.file);
	}
});


router.post('/upload', function (req, res) {

	if(!req.body.title) { res.json({error: "title is required."}); return; }
	if(!req.body.file) { res.json({error: "file is required."}); return; }

	Users.findOne({"auth.token": req.body.token}, function (err, data) {
		if(data) {
			
			// Add photo to database
		 	var newPhoto 	 = new Gallery();

			newPhoto.title 	 = req.body.title;
			newPhoto.artist  = data._id;
			newPhoto.caption = req.body.caption;
			newPhoto.path    = req.body.file;
			newPhoto.tags	 = req.body.tags;
			
			newPhoto.save(function (err, doc) {

				if(err) {
					res.json({error: "Error saving file to database. Try again."});
					return;
				}
				res.json({id: doc._id, message: doc.title + " uploaded successfully."});
			})
		} else {
			removeImage(req.body.file);
			res.json({error: "Not authed. Deleting file."})
		}
	});
});

// Change caption
router.post('/update', isTokenExpired, function (req, res) {

	if(!req.body.photo_id) { res.json({error: "photo_id required."}); return; }
	if(!req.body.caption) { res.json({error: "caption required."}); return; }

	// Find photo by ID.
	Gallery.findOne({_id: toObject(req.body.photo_id)}, function (err, doc) {
		if(err) {
			res.json({error: "Server error has occurred."});
		} else {
			if(doc) {
				// Check if user is artist.
				if (doc.artist.toString() == req.user._id.toString()) {
					// Update caption
					doc.caption = req.body.caption;

					doc.save(function (err) {
						if(err) {
							res.json({error: "Caption could not be updated."});
						} else {
							res.json({message: "Caption updated successfully."});
						}
					})
				} else {
					res.json({error: "Not authorized to update this photo."});
				}
			} else {
				res.json({error: "Photo not found."});
			}
		}
	});
});

// TODO: Do images have comments?

// Remove photo
router.post('/remove', isTokenExpired, function (req, res) {
	if (!req.body.photo_id) { res.json({error: "photo_id is required."}); return; }

	// find photo
	Gallery.findById(toObject(req.body.photo_id), function (err, doc) {
		if(err) {
			res.json({error: "A server error has occurred."});
		}

		if(doc) {
			// Does photo belong to user?
			if(doc.artist.toString() == req.user._id.toString()) {
				doc.remove(function (err) {
					if(err) {
						res.json({error: "A server error has occurred."})
					} else {
						res.json({message: "Photo has been removed."});
					}
				});
			} else {
				res.json({error: "Not authorized to remove photo."});
			}
		} else {
			res.json({error: "Photo not found."});
		}
	})
});

router.get('/getLatest', function (req, res) {
	// Maybe when more popular, we can show results based on location.

	var query = Gallery.find({}).sort({'_id': -1}).limit(15);

	query.exec(function (err, data) {
		if(err) { return res.json({ error: err.message}) };

		if(!data) {
			return res.json({message: false});
		} else {
			return res.json(data);
		}
	})
})


function removeImage(file) {
	fs.exists('./uploads/' + file, function(exists) {
		console.log(exists);
		if(exists) {
			fs.unlink('./cdn/gallery/' + file);
		}
	});
};

// Convert string to mongo object 
function toObject(object_id) {
	try {
		return mongoose.Types.ObjectId(object_id);
	} catch(ex) {
		return ex;
	}
}

function isTokenExpired(req, res, next) {
	if(!req.body.token) { return res.json({error: "token required."}) }

	Users.isTokenValid(req.body.token, function(err, data) {
		if(data) {
			next();
		} else {
			return res.json({message: "Invalid token."});
		}
	});
};

module.exports = router;