// Showcased.Ink Web Services
// File Description: Manage API keys.
// Notes: These API keys should only be for internal developers only. Public APIs are out-of-scope currently.
// Created Jan 13, 2015

var express 	= require('express');
var router 		= express.Router();
var Developers 	= require('../../models/developers');
var bcrypt		= require('bcrypt');
var errorCodes  = require('../errorCodes');

// POST /getDetails
router.post('/getDetails', function (req, res) {
	try {

    	//Validate params
    	if(!req.body.email) {
    		res.send(errorCodes.BAD_REQUEST, "Missing 'email' paramater.");
    	}

    	if(!req.body.password) {
    		res.send(errorCodes.BAD_REQUEST, "Missing 'password' parameter.")
    	}

    	Developers.findOne({email: req.body.email}, function(err, obj) {
            if(obj != null) {
        		// If email exists, compare password
        		bcrypt.compare(req.body.password, obj.password, function(err, result) {
        			if(!result) {
        				res.send(errorCodes.UNAUTHORIZED, "Invalid credentials.")
        			}
        			res.json({
        				name: obj.name,
        				key: obj.key,
                        active: obj.active
        			});
        		})
            } else {
                res.send(errorCodes.UNAUTHORIZED, "Invalid credentials.")
            }
    	})

	} catch(ex) {
		res.send(errorCodes.GENERAL, ex.message);
	}
});

// POST /register
router.post('/register', function (req, res) {
    try {

    	//Validate params
    	if(!req.body.name) {
    		res.send(errorCodes.BAD_REQUEST, "Missing 'name' paramater.");
    	}

    	if(!req.body.email) {
    		res.send(errorCodes.BAD_REQUEST, "Missing 'email' paramater.");
    	}

    	if(!req.body.domains) {
    		res.send(errorCodes.BAD_REQUEST, "Missing 'domains' array paramater.");
    	}

    	if(!req.body.password) {
    		res.send(errorCodes.BAD_REQUEST, "Missing 'password' parameter.")
    	}

    	var Developer = new Developers();


    	// Verify if email already exists
    	Developers.findOne({email: req.body.email}, function(err, obj) {
    		if(obj != null) {
    			res.send(errorCodes.GENERAL, {Error: "Email already exists."});
    		}
    	})

    	Developer.name = req.body.name;
    	Developer.email = req.body.email;

    	Developer.domains = req.body.domains.split(",");

		// Hash the devKey and password with a salt
		var salt = bcrypt.genSaltSync(12);
		var hash = bcrypt.hashSync(req.body.name + Date.now(), salt);
		var passHash = bcrypt.hashSync(req.body.password, salt);

    	Developer.key = hash.split("$")[3].replace(/[^a-z0-9\s]/gi, '');
    	Developer.password = passHash;

    	// Save the developer details
    	Developer.save(function(err) {
    		if(err) {
    			res.send(errorCodes.GENERAL, err);
    		} else {
    			res.json({
                    devkey: Developer.key
                    });
    		}
    	})

    } catch(ex) {
    	res.send(errorCodes.GENERAL, ex.message);
    	console.log(ex);
    }
});

var isTokenExpired = function(auth) {
    if(auth.expiration < Date.now()) {
        return false;
    } else {
        return true;
    }
}


module.exports = router;
