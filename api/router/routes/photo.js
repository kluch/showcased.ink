// Showcased.Ink Web Services
// File Description: Photo methods
// Created May 9, 2015

var express 	= require('express');	
var router 		= express.Router();
var Users 		= require('../../models/users');
var Gallery		= require('../../models/gallery');

router.get('/:photoId/getDetails', function (req, res) {
	Gallery.findOne({"_id": req.param('photoId')})
	.populate('artist', 'fName lName artist')
	.populate('comments.author', 'fName lName artist')
	.exec(function (err, data) {
		res.json(data);
	})
});

router.post('/:photoId/addComment', isTokenExpired, function (req, res) {
	if (!req.body.comment) { return res.json({error: "comment required."})};

	Gallery.findOne({"_id": req.param('photoId')}, function (err, data) {
		if (!data) { return res.json({"error": "Photo not found." })};

		Users.update({"_id": req.body.user.id}, { $inc: { 'meta.commentCount': 1 }}, function (err, data) {
		});

		data.comments.push({ text: req.body.comment, author: req.body.user.id });

		data.save(function (err, doc) {
			return res.json(doc);
		})
	});
});

function isTokenExpired(req, res, next) {
	if(!req.body.token) { return res.json({error: "token required."}) }

	Users.isTokenValid(req.body.token, function(err, data) {
		if(data) {
			Users.findOne({"auth.token": req.body.token}, function (err, data) {
				req.body.user = {id: data._id, fName: data.fName, lName: data.lName};
				next();
			});
		} else {
			return res.json({message: "Invalid token."});
		}
	});
}

module.exports = router;