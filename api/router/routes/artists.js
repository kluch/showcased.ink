// Showcased.Ink Web Services
// File Description: Artist management methods
// Created Feb 16, 2015

var express 	= require('express');	
var router 		= express.Router();
var Users 		= require('../../models/users');
var Gallery 	= require('../../models/gallery');
var Shops 		= require('../../models/shops');
var errorCodes  = require('../errorCodes');

// Convert to artist.
router.post('/convert', isTokenExpired, function (req, res) {
	
		// Validation
		if(!req.body.address1) { res.json({error: "address1 required."}); return; }
		if(!req.body.city) { res.json({error: "city required."}); return; }
		if(!req.body.state) { res.json({error: "state required."}); return; }
		if(!req.body.url) { res.json({error: "url required."}); return; }

		
			Users.findOne({"auth.token": req.body.token}, function(err, data) {

			if(!data) {
				return res.json({error: "Artist not found."});
			}

			if(data.artist.isArtist) { return res.json({error: "You are already an artist."}) };

			Users.isUrlTaken(req.body.url, function (err, result) {
				if(!result) {
					data.artist = {
						isArtist: true,
						nickname: req.body.nickname,
						url: req.body.url.toLowerCase(),
						address: {
							address1: req.body.address1,
							address2: req.body.address2,
							city: req.body.city,
							state: req.body.state,
							zip: req.body.zip
						},
						socialMedia: {
							instagram: req.body.instagram,
							facebook: req.body.facebook,
							twitter: req.body.twitter
						}
					}
				data.save(function(err, doc) {
					return res.json({
						message: "Conversion successful.",
						url: doc.artist.url
					})
				});
			} else {
				return res.json({error: "URL taken."});
			}
		});
	});
});

router.post('/isUrlTaken', function (req, res) {
	Users.isUrlTaken(req.body.url, function(data) {
		if(data) {
			return res.json({message: true});
		} else {
			return res.json({message: false});
		}
	})
})

// Downgrade to regular user
router.post('/downgrade', isTokenExpired, function (req, res) {
	Users.update({"auth.token": req.body.token},
	{
		artist: {
			isArtist: false
		}
	}, function (err, rowsAffected, result) {
		if(err) {
			res.json({error: err.message});
		}

		if(rowsAffected > 0) {
				res.json({message: "Downgrade successful."});
				req.session.destroy();
			}
	});
});

router.post('/getArtist', function (req, res) {
	if (!req.body.artist_url) { return res.json({error: "artist_url is required."}) };

	Users.findOne({"artist.url": req.body.artist_url}, function(err, data) {

		if(!data) {
			return res.json({error: "Artist not found."});
		}	

		return res.json(data);
	});
});

router.post('/isMe', isTokenExpired, function (req, res) {
	if(!req.body.artist_id) { return res.json({error: "artist_id required."}) };

	Users.findOne({"auth.token": req.body.token}, function(err, data) {
		if(err) { return res.json({ error: "An error occurred."}) };

		if(!data) { 
			return res.json({ message: false });
		}

		if(data) {
			if(data.artist.url == req.body.artist_id) {
				return res.json({ message: true });
			} 
			else {
				return res.json({ message: false });
			}
		}
	});
});

router.post('/update/nickname', isTokenExpired, function (req, res) {
	if (!req.body.nickname) { return res.json({error: "nickname required."}) };

	Users.findOne({"auth.token": req.body.token}, function(err, data) {
		if (err) { return res.json({error: "Could not update nickname"}) };

		if(data) {
			data.artist.nickname = req.body.nickname;

			data.save(function (error, doc) {

				if(error) { 
					return res.json({error: error.message}); 
				} else {
					return res.json({message: true});
				}
			});
		}
	});
});

router.post('/update/bio', isTokenExpired, function (req, res) {
	if (!req.body.bio) { return res.json({error: "bio required."}) };

	Users.findOne({"auth.token": req.body.token}, function(err, data) {
		if (err) { return res.json({error: "Could not update bio"}) };

		if(data) {
			data.artist.bio = req.body.bio;

			data.save(function (error, doc) {

				if(error) { 
					return res.json({error: error.message}); 
				} else {
					return res.json({message: true});
				}
			});
		}
	});
});

router.get('/:id/getImages', function (req, res) {

	Gallery.find({'artist': req.param('id')}, function (err, data) {
		return res.json({count: data.length, images: data});
	})
})

router.post('/upload-photo', function (req, res) {
	if(req.files.file) {
		//return res.json({message: "Success!"});
		return res.json(req.files.file);
	}
});

router.post('/change-photo', isTokenExpired, function (req, res) {

	if(!req.body.file) {
		return res.json({"error": "File required."});
	}

	Users.findOne({'_id': req.body.user.id}, function (err, data) {

		data.artist.photo = req.body.file;

		data.save(function (err, data) {
			return res.json({"url": data.artist.url});
		});
	});
});

router.post('/:id/addReview', isTokenExpired, function (req, res) {
	Users.findOne({"artist.url": req.param('id')}, function (err, data) {
		if(data) {
			review = {
				author: {
					id: req.body.user.id,
					fName: req.body.user.fName,
					lName: req.body.user.lName
				},
				text: req.body.review.text,
				professionalism: req.body.review.pro,
				cleanliness: req.body.review.clean,
				quality: req.body.review.quality,
				cost: req.body.review.cost,
				overall: req.body.review.overall,
				date: Date.now()
				};
				
				data.artist.reviews.push(review);

				data.save(function (err, data) {
					return res.json(data);
				})
			}
		});
})

function isTokenExpired(req, res, next) {
	if(!req.body.token) { return res.json({error: "token required."}) }

	Users.isTokenValid(req.body.token, function(err, data) {
		if(data) {
			Users.findOne({"auth.token": req.body.token}, function (err, data) {
				req.body.user = {id: data._id, fName: data.fName, lName: data.lName};
				next();
			});
		} else {
			return res.json({message: "Invalid token."});
		}
	});
}


module.exports = router;