// Showcased.Ink Web Services
// File Description: User authentication and registration methods.
// Created Jan 26, 2015

var express 	= require('express');	
var router 		= express.Router();
var bcrypt 		= require('bcrypt');
var Users 		= require('../../models/users');
var Shops 		= require('../../models/shops');
var Gallery		= require('../../models/gallery');
var errorCodes  = require('../errorCodes');
var passport    = require('passport');

//POST /register
router.post('/register', function (req, res) {

	// TOOD: validate required fields.
	if (!req.body.email) {
		res.json({"error":"Email required."})
		return;
	}

	if (!req.body.fName) {
		res.json({"error":"fName required."})
		return;
	}

	if (!req.body.lName) {
		res.json({"error":"lName required."})
		return;
	}

	if (!req.body.password) {
		res.json({"error":"Password required."})
		return;
	}

	if(!req.body.birthday) {
		res.json({"error":"Birthday required."})
		return;
	}

	// Check if account exists

	Users.userExists(req.body.email, function (err, obj) {
		if(err) {
			res.send(errorCodes.GENERAL, "Server error occurred.");
			return;
		}

		if(obj != null) {
			res.json({error: "user already exists."});
			return;
		} 
		else {
			// Create new account object.
			var NewUser = Users();

			// Assign values to object.
			NewUser.email 	 = req.body.email;
			NewUser.fName 	 = req.body.fName;
			NewUser.lName	 = req.body.lName;
			NewUser.birthday = new Date(req.body.birthday);

			// bcrypt password.
			var salt = bcrypt.genSaltSync(12);
			var pass = bcrypt.hashSync(req.body.password, salt);

			NewUser.password = pass;

			// Authenticate User 
			var date = new Date();

			NewUser.save(function (err, doc) {
				res.json(doc);
			});
		}
	})
})

// POST /login - Local auth
router.post('/login', function (req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) { return next(err); }
    if (!user) { return res.json({error: "Invalid email/password."})};
    req.logIn(user, function(err) {
		if (err) { return next(err); }

	  	var salt = bcrypt.genSaltSync(12);
		var token = bcrypt.hashSync(Date.now().toString(), salt);

	  	user.auth.token = token;

	  	var date = new Date();

	    if(req.body.rememberMe) {
	      	user.auth.expiration = date.setFullYear(date.getFullYear() + 1);
	    } else {
	      	user.auth.expiration = date.setDate(date.getDate() + 1);
	    }
	    
	    user.save(function(err) {
	    	if(err) { return res.json({error: "Auth failed."}) };
	    	return res.json({message: user.auth.token});
	    })
	
    });
  })(req, res, next);

});

// POST /login/fb - Facebook connect
router.get('/login/fb', function (req, res, next) {
  passport.authenticate('facebook', function(err, user, info) {
    if (err) { return next(err); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return res.json({message: "Authed."});
    });
  })(req, res, next);
});

// GET /logout
router.get('/logout', function (req, res) {
	req.logOut();
	req.session.destroy();

	res.json({message: "Session ended."})
});

// GET /details
router.post('/details', isTokenExpired, function (req, res) {
	Users.findOne({"auth.token": req.body.token}, function(err, data) {
		if(err) { return res.json({error: err.message}) };

		if(data) {
			res.json({ 
				id: data._id,
				fName: data.fName,
				lName: data.lName,
				lastInitial: data.lName[0],
				email: data.email,
				isArtist: data.artist.isArtist,
				meta: data.meta,
				customUrl: data.artist.url
			});
		} 
	})
});

router.get('/:id/getShops', function (req, res) {
	Shops.find({ $or: [{'adminList': req.param('id')}, {'artists': req.param('id')}]}).lean().select('name customUrl artists adminList').exec(function (err, data) {
		for (entry in data) {

			// Is user an admin or an artist?
			for (admin in data[entry].adminList) {
				if(data[entry].adminList[admin].toString() == req.param('id').toString()) {
					data[entry].isAdmin = true;
				}
			}

			for (artist in data[entry].artists) {
				if(data[entry].artists[artist].toString() == req.param('id').toString()) {
					data[entry].isArtist = true;
				}
			}
		}

		return res.json(data);
	});
})
// ROUTING FUNCTIONS

function isTokenExpired(req, res, next) {
	if(!req.body.token) { return res.json({error: "token required."}) }

	Users.isTokenValid(req.body.token, function(err, data) {
		if(data) {
			Users.findOne({"auth.token": req.body.token}, function (err, data) {
				req.body.user = {id: data._id, fName: data.fName, lName: data.lName};
			});

			next();
		} else {
			return res.json({message: "Invalid token."});
		}
	});
}

module.exports = router;
