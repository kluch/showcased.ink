// Server Error codes

module.exports = {
	BAD_REQUEST		: 400,
	FORBIDDEN		: 403,
    GENERAL			: 500,
    UNAUTHORIZED	: 401
};