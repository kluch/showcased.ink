// Showcased.Ink
// Created: 1/14/2015
// Description: Various functions for API.

var Developers 	= require('../models/developers');
var errorCodes  = require('../errorCodes');

// Allow requests from these IPs
var whiteList = ['127.0.0.1', '::1', '73.13.158.135', '172.16.0.1'];

module.exports = {
	//	Verify whether the dev token is valid and active
	isKeyValid: function(key, connection) {
	    Developers.isKeyValid(key, function(err, obj) {
	        if (err) {
	            res.send(errorCodes.GENERAL, "Server error occurred.")
	        }
	        
	        if(obj == null) {
	            res.send(errorCodes.UNAUTHORIZED, "Dev key not valid.")
	        } else if(!obj.active) {
	            res.send(errorCodes.UNAUTHORIZED, "Dev key disabled.")
	        } else {
	            return true;
	        }
	    })
	},
	privateMode: function(req, res) { // When private mode is enabled, dev keys are pointless.
		if(whiteList.indexOf(req.connection.remoteAddress) > -1) {
			return true;
		} else {
			return false;
		}
	},
	uploadLimit: 5 // in megabytes
}