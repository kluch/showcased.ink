var os 		 = require("os");
var mongoose = require("mongoose");

module.exports = {
	connectDb: function() {
		if(os.hostname() == "showcased.ink") {
			mongoose.connect('mongodb://sciUser:{{PASSWORD}}@mongo-01.showcased.ink:27017/sci-beta');
		} else {
			mongoose.connect('mongodb://sciUser:{{PASSWORD}}@mongo-01.showcased.ink:27017/sci-dev');
		}
	}
}