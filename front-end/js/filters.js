// FILTERS

var app = angular.module('sciApp');

// Convert line breaks to html (for reviews).
app.filter('breakFilter', function () {
    return function (text) {
        if (text !== undefined) return text.replace(/\n/g, '<br />');
    };
});

app.filter('unsafe', function($sce) { return $sce.trustAsHtml; });