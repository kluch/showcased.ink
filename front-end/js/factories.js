//  FACTORIES/SERVICES

var app = angular.module('sciApp');

// User Services
app.factory('userService', function ($http, $q) {

	return {
		Details: function(authToken) {
			var deferred = $q.defer();

		    $http({
			url: "http://"+apiURL+":8080/accounts/details",
			method: "POST",
			data: {
				"token": authToken
			}
			}).success(function(data){
	   	 		if(data.error) {
	   	 			deferred.resolve(data);
	   	 		} else {
	   	 			deferred.resolve(data); 	 		
	   	 		}
	  		}).error(function(data) {
	  			// Error handling
	  			deferred.resolve(data);
	  		});

  		return deferred.promise;
		},
		Login: function(email, password) {
			var deferred = $q.defer();

		    $http({
			url: "http://"+apiURL+":8080/accounts/login",
			method: "POST",
			data: {
				"email": email,
				"password": password
			}
			}).success(function(data){
	   	 		if(data.error) {
	   	 			deferred.resolve(data);
	   	 		} else {
	   	 			deferred.resolve(data); 	 		
	   	 		}
	  		}).error(function(data) {
	  			console.log(data.message);
	  		});

  		return deferred.promise;
		},
		Register: function(email, password, fName, lName, confirmPassword, birthday) {
			var deferred = $q.defer();

		    $http({
			url: "http://"+apiURL+":8080/accounts/register",
			method: "POST",
			data: {
				"email": email,
				"password": password,
				"birthday": birthday,
				"fName": fName,
				"lName": lName
			}
			}).success(function(data){
	   	 		if(data.error) {
	   	 			deferred.resolve(data);
	   	 		} else {
	   	 			deferred.resolve(data); 	 		
	   	 		}
	  		}).error(function(data) {
	  			console.log(data);
	  		});

  		return deferred.promise;
		},
		Logout: function() {
			var deferred = $q.defer();

		    $http({
			url: "http://"+apiURL+":8080/accounts/logout",
			method: "GET",
			}).success(function(data){
	   	 		if(data.error) {
	   	 			deferred.resolve(data);
	   	 		} else {
	   	 			deferred.resolve(data); 	 		
	   	 		}
	  		}).error(function(data) {
	  			console.log(data);
	  		});

  			return deferred.promise;
  		},
  		getShops: function(user) {
  			var deferred = $q.defer();

		    $http({
			url: "http://"+apiURL+":8080/accounts/"+ user +"/getShops",
			method: "GET",
			}).success(function(data){
	   	 		if(data.error) {
	   	 			deferred.resolve(data);
	   	 		} else {
	   	 			deferred.resolve(data);  		
	   	 		}
	  		}).error(function(data) {
	  			console.log(data);
	  		});

  			return deferred.promise;
  		}
	}
});

// Artist Services
app.factory('artistService', function ($http, $q) {
	return {
		getArtistInfo: function(url) {
			var deferred = $q.defer();

		 	$http({
				url: "http://"+apiURL+":8080/artists/getArtist",
				method: "POST",
				data: {
					"artist_url": url
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(error) {
				deferred.resolve(error);
			});

			return deferred.promise;
		},
		isMe : function(authToken, url) {
			var deferred = $q.defer();

			$http({
				url: "http://"+apiURL+":8080/artists/isMe",
				method: "POST",
				data: {
					"token": authToken,
					"artist_id": url
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		updateNick: function(authToken, nickname) {
			var deferred = $q.defer();

			$http({
				url: "http://"+apiURL+":8080/artists/update/nickname",
				method: "POST",
				data: {
					"token": authToken,
					"nickname": nickname
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		updateBio: function(authToken, bio) {
			var deferred = $q.defer();

			$http({
				url: "http://"+apiURL+":8080/artists/update/bio",
				method: "POST",
				data: {
					"token": authToken,
					"bio": bio
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		getImages: function(artist_id) {
			var deferred = $q.defer();

			$http({
				url: "http://"+apiURL+":8080/artists/"+ artist_id +"/getImages",
				method: "GET"
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		changeProfileImage: function(authToken, file) {
			var deferred = $q.defer();

			$http({
				url: "http://"+apiURL+":8080/artists/change-photo",
				method: 'POST',
				data: {
					token: authToken,
					file: file
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		addReview: function(authToken, review, artist) {
			var deferred = $q.defer();

			$http({
				url: "http://"+apiURL+":8080/artists/"+artist+"/addReview",
				method: 'POST',
				data: {
					token: authToken,
					review: review
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		}
	}
});

app.factory('profileService', function ($http, $q) {
	return {
		getInformation: function(user_id) {
			var deferred = $q.defer();

			$http({
				method: 'POST',
				url: "http://"+apiURL+":8080/profiles/getInformation",
				data: {
					"user_id": user_id
				}
			}).success(function(result) {
				deferred.resolve(result);
			}).error(function(result) {
				deferred.resolve({error: "an error occurred."});
			});

			return deferred.promise;
		},
		isMe: function(authToken, user_id) {
			var deferred = $q.defer();

			$http({
				method: 'POST',
				url: "http://"+apiURL+":8080/profiles/isMe",
				data: {
					"token": authToken,
					"user_id": user_id
				}
			}).success(function(result) {
				deferred.resolve(result);
			}).error(function(result) {
				deferred.resolve(result);
			});

			return deferred.promise;
		},
		convert: function(authToken, form) {
			var deferred = $q.defer();

			$http({
				method: 'POST',
				url: "http://"+apiURL+":8080/artists/convert",
				data: {
					"token": authToken,
					"address1": form.addr,
					"city": form.city,
					"state": form.state,
					"zip": form.zip,
					"url": form.url
				}
			}).success(function (results) {
				deferred.resolve(results);
			}).error(function (results) {
				deferred.resolve(results);
			});

			return deferred.promise;
		},
		getReviews: function(id) {
			var deferred = $q.defer();

			$http({
				method: 'GET',
				url: "http://"+apiURL+":8080/profiles/"+id+"/allReviews"
			}).success(function (results) {
				deferred.resolve(results);
			}).error(function (results) {
				deferred.resolve(results);
			});

			return deferred.promise;
		},
		getAllReviews: function() {
			var deferred = $q.defer();

			$http({
				method: 'GET',
				url: "http://"+apiURL+":8080/profiles/allReviews"
			}).success(function (results) {
				deferred.resolve(results);
			}).error(function (results) {
				deferred.resolve(results);
			});

			return deferred.promise;
		}
	}
});

app.factory('uploadService', function ($http, $q) {
	return {
		galleryImage: function(authToken, title, file, caption, tags) {
			var deferred = $q.defer();

			$http({
				method: "POST",
				url: "http://"+apiURL+":8080/gallery/upload",
				data: {
					token: authToken,
					file: file,
					title: title,
					caption: caption,
					tags: tags
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});
	
			return deferred.promise;
		}
	}
});

app.factory('galleryService', function ($http, $q) {
	return {
		getLatest: function() {
			var deferred = $q.defer();

			$http({
				method: "GET",
				url: "http://"+apiURL+":8080/gallery/getLatest"
			}).success(function (data) {
				deferred.resolve(data);
			}).error(function (data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		}
	};
});

app.factory('searchService', function ($http, $q){
	return {
		getArtists: function(name) {
			var deferred = $q.defer();

			$http({
				method: "GET",
				url: "http://"+apiURL+":8080/search/artists/" + name
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		getTags: function(tag) {
			var deferred = $q.defer();

			$http({
				method: "GET",
				url: "http://"+apiURL+":8080/search/tags/" + tag
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		getShops: function(shop) {
			var deferred = $q.defer();

			$http({
				method: "GET",
				url: "http://"+apiURL+":8080/search/shops/" + shop
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		}
	};
});

app.factory('photoService', function ($http, $q){
	return {
		getDetails: function(photoId) {
			var deferred = $q.defer();

			$http({
				method: 'GET',
				url: 'http://' + apiURL + ':8080/photo/' + photoId + '/getDetails'
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		addComment: function(authToken, photoId, text) {
			var deferred = $q.defer();

			$http({
				method: 'POST',
				url: 'http://' + apiURL + ':8080/photo/' + photoId + '/addComment',
				data: {
					token: authToken,
					comment: text
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		imgurUpload: function(img) {
			var deferred  = $q.defer();

			$http({
				method: 'GET',
				url: "http://uploads.im/api?upload="+ img,
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		}
	}
});

app.factory('shopService', function ($http, $q) {
	return {
		createShop: function(authToken, fileName, shopInfo) {
			var deferred = $q.defer();

			$http({
				method: 'POST',
				url: 'http://' + apiURL + ':8080/shops/create',
				data: {
					token: authToken,
					newShop: shopInfo,
					file: fileName
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			})

			return deferred.promise;
		},
		updateShop: function(authToken, fileName, shopInfo, url) {
			var deferred = $q.defer();

			$http({
				method: 'POST',
				url: 'http://' + apiURL + ':8080/shops/' + url + '/update',
				data: {
					token: authToken,
					newData: shopInfo,
					file: fileName
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			})

			return deferred.promise;
		},
		getDetails: function(url) {
			var deferred = $q.defer();

			$http({
				method: "GET",
				url: 'http://' + apiURL + ':8080/shops/details/' + url
			}).success(function (data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		isAdmin: function(authToken, url) {
			var deferred = $q.defer();

			$http({
				method: "POST",
				url: 'http://' + apiURL + ':8080/shops/isadmin/' + url,
				data: {
					token: authToken
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		saveHours: function(authToken, hours, url) {
			var deferred = $q.defer();

			$http({
				method: "POST",
				url: 'http://' + apiURL + ':8080/shops/' + url + '/hours',
				data: {
					"token": authToken,
					"hours": hours
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		saveGlance: function(authToken, glance, url) {
			var deferred = $q.defer();

			$http({
				method: "POST",
				url: 'http://' + apiURL + ':8080/shops/' + url + '/glance',
				data: {
					"token": authToken,
					"glance": glance
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		addArtist: function(authToken, artist, url) {
			var deferred = $q.defer();

			$http({
				method: "POST",
				url: 'http://' + apiURL + ':8080/shops/' + url + '/add/artist',
				data: {
					"token": authToken,
					"artistId": artist
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		removeArtist: function(authToken, artist, url) {
			var deferred = $q.defer();

			$http({
				method: "POST",
				url: 'http://' + apiURL + ':8080/shops/' + url + '/remove/artist',
				data: {
					"token": authToken,
					"artistId": artist
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		addAdmin: function(authToken, admin, url) {
			var deferred = $q.defer();

			$http({
				method: "POST",
				url: 'http://' + apiURL + ':8080/shops/' + url + '/add/admin',
				data: {
					"token": authToken,
					"adminId": admin
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		removeAdmin: function(authToken, admin, url) {
			var deferred = $q.defer();

			$http({
				method: "POST",
				url: 'http://' + apiURL + ':8080/shops/' + url + '/remove/admin',
				data: {
					"token": authToken,
					"adminId": admin
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		addReview: function(authToken, review, url) {
			var deferred = $q.defer();

			$http({
				method: "POST",
				url: 'http://' + apiURL + ':8080/shops/' + url + '/review',
				data: {
					"token": authToken,
					"review": review
				}
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		}
	}
});

app.factory('staffService', function ($http, $q) {
	return {
		
		getUnauthorizedShops: function(authToken) {
			var deferred = $q.defer();

			$http({
				method: 'GET',
				url: 'http://' + apiURL + ':8080/staff/unauthorizedshops'
			}).success(function(data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		},
		approveShop: function(authToken, shopID) {
			var deferred = $q.defer();

			$http({
				method: "POST",
				url: 'http://' + apiURL + ':8080/staff/approveshop/' + shopID,
				data: {
					token: authToken,
					shopID: shopID
				}
			}).success(function (data) {
				deferred.resolve(data);
			}).error(function(data) {
				deferred.resolve(data);
			});

			return deferred.promise;
		}
	}
});