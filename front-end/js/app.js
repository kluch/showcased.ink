/**
* SCI Module
*
* SCI API calls
*/

var app = angular.module('sciApp', ['ngRoute', 'flow']);

//Change IP based on url location
var apiURL 	= window.location.host;
var cdnPort = 8888; // Gallery server port.

app.run(function ($rootScope, $location, $window, userService) {

	$rootScope.isAuthed = false;

	$rootScope.hideModal = true;

	// Detect back and forward history buttons.
	$rootScope.$on('$locationChangeSuccess', function() {
        $rootScope.actualLocation = $location.path();
    });        

	// Watch for back and forward history button change.
   $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
        if($rootScope.actualLocation === newLocation) {
			$window.location.reload();
        }
    });

   $window.fbAsyncInit = function() {
	    FB.init({ 
	      appId: '784558704933296',
	      status: true, 
	      cookie: true, 
	      xfbml: true,
	      version: 'v2.3'
	    });
	}
});

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	
	$routeProvider.when('/', {
		templateUrl: '/templates/dashboard.html',
		controller: 'dashboardController'
	});
	
	$routeProvider.when('/artist/change_picture', {
		templateUrl: '/templates/change_picture.html',
		controller: 'artistController'
	});

	$routeProvider.when('/artist/:artistId', {
		templateUrl: '/templates/artist.html',
		controller: 'artistController'
	});

	$routeProvider.when('/shop/create', {
		templateUrl: '/templates/shops/create_shop.html',
		controller: 'shopController'
	});

	$routeProvider.when('/shop/success/:shopId', {
		templateUrl: '/templates/shops/success.html',
		controller: 'shopController'
	});

	$routeProvider.when('/shop/:shopId', {
		templateUrl: '/templates/shops/shop.html',
		controller: 'shopController'
	});

	$routeProvider.when('/shop/:shopId/manage', {
		templateUrl: '/templates/shops/manage.html',
		controller: 'shopController'
	});

	$routeProvider.when('/profile/convert', {
		templateUrl: '/templates/convert.html',
		controller: 'profileController'
	});
	
	$routeProvider.when('/profile/:user', {
		templateUrl: '/templates/profile.html',
		controller: 'profileController'
	});

	$routeProvider.when('/search/:keyword', {
		templateUrl: '/templates/search.html',
		controller: 'searchController'
	});

	$routeProvider.when('/photo/:photoId', {
		templateUrl: '/templates/photo.html',
		controller: 'photoController'
	});

	$routeProvider.otherwise({redirectTo: '/'});
}]);
