$(document).ready(function() {

	$(window).stellar({horizontalScrolling: false});

	// Prevent default anchor action
	$( 'a[href="#"]' ).click( function(e) {
      e.preventDefault();
   	});

	$("#loginButton").click(function() {
		$("#email").focus();
	});
});