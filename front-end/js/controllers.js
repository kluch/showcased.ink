// CONTROLLERS

var app = angular.module('sciApp');

// Authentication
app.controller('authUser', function ($scope, $routeParams, $rootScope, $route, $location, userService) {


	$scope.url 		= apiURL;
	$scope.cdnPort 	= cdnPort;
	
	$scope.hideModal = $rootScope.hideModal;

	$scope.reload = function() {
 		$route.reload();
	}	

	$scope.getUser = function() {
		return $rootScope.user;
	}

	$scope.getUserUrl = function() {
		if($scope.user.isArtist) {
			$location.url('/artist/' + $scope.user.customUrl);
		} else {
			$location.url('/profile/' + $scope.user.id);
		}
	}

	$scope.getAuth = function() {
		var detailPromise = userService.Details(sessionStorage.authToken);
		detailPromise.then(function(data) {

		 	if(data.error) {
				$rootScope.isAuthed = false;
		 	} else {
		 		$rootScope.isAuthed = true;
				$scope.user = data;
		 	}
		});
	}

	$scope.login = function() {

		var promise = userService.Login($scope.loginForm.email, $scope.loginForm.password);

		promise.then(function(data) {
		 	if(data.error) {
		 		$scope.errorText = data.error;
		 	}
		 	
		 	if(data.message) {
		 		$scope.hideModal = true;
		 		sessionStorage.authToken = data.message;
		 		$rootScope.isAuthed = true;
		 		$scope.getAuth();
		 		$route.reload();
		 	}
		});
	}

	$scope.logout = function() {
		var promise = userService.Logout();

		promise.then(function(data) {
			sessionStorage.clear();
			$rootScope.isAuthed = false;
			$route.reload();
		});
	}

	$scope.register = function() {

		var bday = new Date($scope.regForm.birthday);

		if (isNaN(bday.valueOf())) {
			$scope.errorText = "Birthday is not valid.";
			return;
		}

		if ($scope.loginForm.password != $scope.regForm.passConfirm) {
			$scope.errorText = "Passwords do not match.";
		} else {
			var promise = userService.Register (
				$scope.loginForm.email, 
				$scope.loginForm.password,
				$scope.regForm.fName,
				$scope.regForm.lName,
				$scope.regForm.confirmPass,
				$scope.regForm.birthday
			);


			promise.then(function(data) {
				if(data.error) {
					$scope.errorText = "Error: " + data.error;
				} else {
					$scope.errorText = '';

					// Go to login.
					$scope.showRegistration = false;

					// Clear forms.
					$scope.loginForm.password  = ''; 
					$scope.regForm = {}; 

				}
			})
		}
		
	}

	// Watch for the modal box.
	$scope.$watch('hideModal', function(val) {
		// Clear form
		$scope.loginForm  = {}; 
		$scope.regForm    = {}; 
		$scope.upload	  = {};
		$scope.errorText = '';
		$scope.errorMsg = '';
   });
});

app.controller('dashboardController', function ($scope, $routeParams, $rootScope, galleryService, userService, profileService){

	$scope.url = apiURL;

	var detailPromise = userService.Details(sessionStorage.authToken);
		detailPromise.then(function(data) {

		 	if(data.error) {
				$scope.isAuthed = false;
		 	} else {
		 		$scope.isAuthed = true;
				$scope.user = data;

				userShops(data.id);
		 	}
		});


	

	$scope.getRecentPhotos = function() {
		var promise = galleryService.getLatest();

		promise.then(function (data) {
			$scope.recentPhotos = data;
		});
	};

	// Get list of shops of which the user is a member.
	function userShops(user) {
		var promise = userService.getShops(user);

		promise.then(function (data) {
			$scope.userShops = data;
		});
	}

	$scope.getAllReviews = function() {
		var promise = profileService.getReviews();

		promise.then(function(data) {
			$scope.recentReviews = data;
		});
	}
});

app.controller('artistController', function ($scope, $routeParams, $rootScope, $window, artistService) {


	$scope.url 		= apiURL;
	$scope.cdnPort 	= cdnPort;
	
	$scope.getArtistInfo = function() {

		var promise = artistService.getArtistInfo($routeParams.artistId);

		promise.then(function (data) {
			if(data.error) {
				$scope.artistFound = false;
			} else {
				$scope.artistFound = true;
				$scope.artistInfo = data;

				var overall_list = _.map(data.artist.reviews, function(results) {
				return results.overall;
				})

				var rating = 0;

				for (x in overall_list) {
					if(overall_list[x] == null) {
						continue;
					} else {
						rating += overall_list[x]
					}
				}

				rating /= overall_list.length;

				$scope.artistInfo.artist.rating = rating;

				newestPhotos(data._id)

			}
		});
	};

	$scope.isMe = function() {
		var promise = artistService.isMe(sessionStorage.authToken, $routeParams.artistId);

		promise.then(function (data) {
			if(data.message) {
				$scope.isMe = true;
			} else {
				$scope.isMe = false;
			}
		})
	};

	$scope.updateNick = function() {
		var promise = artistService.updateNick(sessionStorage.authToken, $scope.artistInfo.artist.nickname);

		promise.then(function (data) {
			if(data.message) {
				$scope.nickEdit = false;
				$scope.getArtistInfo();
			}
		});
	};

	$scope.updateBio = function() {
		var promise = artistService.updateBio(sessionStorage.authToken, $scope.artistInfo.artist.bio);

		promise.then(function(data) {
			$scope.bioEdit = false;
			$scope.getArtistInfo();
		});
	};

	$scope.addReview = function() {
		var shopPromise = artistService.addReview(sessionStorage.authToken, $scope.review, $routeParams.artistId);

		shopPromise.then(function (data) {
			if(data) {
				$window.location.reload();
			} else {
				// Error handling.
			}
		});
	}

	function newestPhotos (id) {
		var promise = artistService.getImages(id);

		promise.then(function(data) {
			$scope.newPhotos = data;
		})
	}

});

app.controller('profileController', function ($scope, $routeParams, $rootScope, $location, profileService) {


	$scope.getProfile = function() {
		var promise = profileService.getInformation($routeParams.user);

		promise.then(function (data) {
			if(data.error) {
				$scope.profileFound = false;
			} else {
				$scope.profileFound = true;
				$scope.profileInfo = data;
			}
		})
	};

	$scope.getCounts = function(user) {
		var promise = profileService.getInformation(user);

		promise.then(function (data) {
			if(data.error) {
			} else {
				$scope.counts = data;
			}
		})
	};

	$scope.isMe = function() {

		var promise = profileService.isMe(sessionStorage.authToken, $routeParams.user);

		promise.then(function (data) {
			if(data.error) {
				$scope.isMe = false;
			}

			if(data.message) {
				$scope.isMe = data.message;
			}
		});
	};

	$scope.convert = function() {
		if($scope.convertForm.$valid) {
			$scope.disable = true;
			var promise = profileService.convert(sessionStorage.authToken, $scope.convertData);

			promise.then(function (data) {
				if(data.error) {
					$scope.errorText = data.error;
					$scope.disable = false;
					$("html, body").animate({ scrollTop: "30px" });
				} else {
					$scope.disable = false;
					$scope.errorText = null;
					$scope.convertResult = data.message;
					$location.path( "/artist/" + data.url);
					location.reload();
				}
			});
		};
	}

	$scope.getAllReviews = function() {
		var promise = profileService.getReviews($routeParams.user);

		promise.then(function(data) {
			console.log(data);
			$scope.recentReviews = data;
		});
	}
});

app.controller('uploadController', function ($scope, $location, $rootScope, $route, $window, uploadService, artistService) {
	
	$scope.$on('flow::fileAdded', function (event, $flow, flowFile) {
		
		$scope.errorText = null;

		if(flowFile.size > 5000000) {
			$scope.errorText = "Error: File too large.";
			//prevent file from uploading
			event.preventDefault();
		} else {
			$flow.upload();	
		}
	});

	$scope.$on('flow::fileSuccess', function (event, $flow, flowFile, message) {
  		$scope.fileName = jQuery.parseJSON(message);
	});

	$scope.profilePhotoUpload = function(photo, flow) {
		var promise = artistService.changeProfileImage(sessionStorage.authToken, $scope.fileName.name);

		promise.then(function(result) {
			$location.path('/artist/' + result.url);
		})
	}

	$scope.complete = function(data, flow) {
	
		var promise = uploadService.galleryImage(sessionStorage.authToken, $scope.upload.title, $scope.fileName.name, $scope.upload.caption, $scope.tags);

		promise.then(function(result) {
			
			if(result.hasOwnProperty('message')) {
				$scope.upload = {};
				$scope.errorMsg = null;
				$scope.errorText = result.message;
				flow.cancel();

				
				$window.location.reload();
				$location.path("/photo/" + result.id);
			} else {
				$scope.errorMsg = result.error;
			}
		})
	};
});

app.controller('searchController', function ($scope, $routeParams, $location, searchService, artistService) {
	$scope.keyword = $routeParams.keyword;

	$scope.url 		= apiURL;
	$scope.cdnPort 	= cdnPort;

	if($location.path().indexOf('search') > 0) {
		getArtists();
		getTags();
		getShops();
	}

	$scope.searchBar = function(keyword, key) {
		if(key == 13) {
			$location.path('/search/' + keyword);
		}
	}

	function getArtists() {
		var promise = searchService.getArtists($routeParams.keyword);

		promise.then(function(data) {
			// Return the number of results
			$scope.artistCount = data.length;

			// Modify the data object to include image information.
			for(x in data) {
			
				var imagePromise = artistService.getImages(data[x]._id);

				imagePromise.then(function (images) {
					if(images.count > 0) {
						data[x].imageInfo = images;
					}
				});

			}
				$scope.artistList = data;
		});
	}

	function getTags() {
		var promise = searchService.getTags($routeParams.keyword);

		promise.then(function(data) {
			// Return the number of results
			$scope.imageCount = data.length;

			$scope.tagResults = data;
		});
	}

	function getShops() {
		var promise = searchService.getShops($routeParams.keyword);

		promise.then(function(data) {
			$scope.shopCount = data.length;

			$scope.shopResults = data;
		});
	}
});

app.controller('photoController', function ($scope, $routeParams, $route, photoService) {
	$scope.url 		= apiURL;
	$scope.cdnPort 	= cdnPort;
	$scope.zoomed	= false;
	
	$scope.getDetails = getDetails();


	function getDetails() {
		var promise = photoService.getDetails($routeParams.photoId);

		promise.then(function(data) {
			$scope.image = data;
		});
	}

	$scope.addComment = function(text) {
		var promise = photoService.addComment(sessionStorage.authToken, $routeParams.photoId, text);

		promise.then(function(data) {
			$scope.input.newComment = "";
			getDetails();
		});
	}

	$scope.sharePinterest = function() {
		var promise = photoService.imgurUpload("http://" + $scope.url + ":" + $scope.cdnPort + "/gallery/" + $scope.image.path);

		promise.then(function(data) {
			$scope.pinClick = false;

			window.open("http://pinterest.com/pin/create/link/?url=http://"+ $scope.url + "/%23/photo/" + $scope.image._id +"&media=" + data.data.img_url + "&description=" + $scope.image.title + "%20by%20" + $scope.image.artist.fName + "%20" + $scope.image.artist.lName, "Pin It", "height=400,width=400");
		});
	}
});

app.controller('shopController', function ($scope, $routeParams, $route, $rootScope, $location, $window, userService, shopService) {


	$scope.url 		= apiURL;
	$scope.cdnPort 	= cdnPort;

	var detailPromise = userService.Details(sessionStorage.authToken);

	detailPromise.then(function(data) {

	 	if(data.error) {
			$scope.isAuthed = false;
	 	} else {
	 		$scope.isAuthed = true;
			$scope.user = data;
	 	}
	});

	if($routeParams.shopId) {
		getShopFromUrl();
		isAdmin();
		// $scope.editHours = false;
	}

	$scope.$on('flow::fileAdded', function (event, $flow, flowFile) {
		$scope.errorText = null;

		if(flowFile.size > 5000000) {
			$scope.errorText = "Error: File too large.";
			//prevent file from uploading
			event.preventDefault();
		} else {
			$flow.upload();	
		}
	});

	$scope.$on('flow::fileSuccess', function (event, $flow, flowFile, message) {
  		$scope.fileName = jQuery.parseJSON(message);
	});

	$scope.createShop = function (data, file, flow) {

		var shopPromise = shopService.createShop(sessionStorage.authToken, $scope.fileName, data);

		shopPromise.then(function(data) {
			if(data.error) {
				$scope.errorText =  data.error;
				$("html, body").animate({ scrollTop: "30px" }, 1000);
			} else {
				$location.path("/shop/success/" + data.customUrl);
			}
		})
	};

	$scope.updateShop = function (shopInfo) {

		var shopPromise = shopService.updateShop(sessionStorage.authToken, $scope.fileName, shopInfo, $routeParams.shopId);

		shopPromise.then(function (data) {
			$route.reload();
		});
	};

	$scope.isShop = function () {
		return $routeParams.shopId ? true : false;
	}

	$scope.clearLicense = function ($flow) {
		$flow.cancel();
	}


	$scope.isOpen = function (day, time) {
		var date = new Date;
		var today = date.getDay();

		
		if (today == day) {
			if(time && time.isOpen) {
				var open = moment(time.open, "h:ia");
				var close = moment(time.close, "h:ia");

				if(moment().isBetween(open, close)) {
					return "open";
				} else {
					return "closed";
				}
			}
		}
	}

	$scope.saveHours = function () {
		if($scope.sunday.open.toLowerCase() == "closed" || $scope.sunday.close.toLowerCase() == "closed") {
			$scope.sunday.isOpen = false;
		} else {
			$scope.sunday.isOpen = true;
		}

		if($scope.monday.open.toLowerCase() == "closed" || $scope.monday.close.toLowerCase() == "closed") {
			$scope.monday.isOpen = false;
		} else {
			$scope.monday.isOpen = true;
		}

		if($scope.tuesday.open.toLowerCase() == "closed" || $scope.tuesday.close.toLowerCase() == "closed") {
			$scope.tuesday.isOpen = false;
		} else {
			$scope.tuesday.isOpen = true;
		}

		if($scope.wednesday.open.toLowerCase() == "closed" || $scope.wednesday.close.toLowerCase() == "closed") {
			$scope.wednesday.isOpen = false;
		} else {
			$scope.wednesday.isOpen = true;
		}

		if($scope.thursday.open.toLowerCase() == "closed" || $scope.thursday.close.toLowerCase() == "closed") {
			$scope.thursday.isOpen = false;
		} else {
			$scope.thursday.isOpen = true;
		}

		if($scope.friday.open.toLowerCase() == "closed" || $scope.friday.close.toLowerCase() == "closed") {
			$scope.friday.isOpen = false;
		} else {
			$scope.friday.isOpen = true;
		}

		if($scope.saturday.open.toLowerCase() == "closed" || $scope.saturday.close.toLowerCase() == "closed") {
			$scope.saturday.isOpen = false;
		} else {
			$scope.saturday.isOpen = true;
		}

		hours = {
			sunday: $scope.sunday,
			monday: $scope.monday,
			tuesday: $scope.tuesday,
			wednesday: $scope.wednesday,
			thursday: $scope.thursday,
			friday: $scope.friday,
			saturday: $scope.saturday
		}

		var shopPromise = shopService.saveHours(sessionStorage.authToken, hours, $routeParams.shopId);

		shopPromise.then(function (data) {
			$scope.editHours = false;

			$route.reload();
		});
	}

	$scope.reload = function() {
		$route.reload();
	}

	$scope.saveGlance = function() {
		var shopPromise = shopService.saveGlance(sessionStorage.authToken, $scope.glance, $routeParams.shopId);

		shopPromise.then(function (data) {
			$route.reload();
		})
	}

	$scope.addArtist = function (artist) {
		var shopPromise = shopService.addArtist(sessionStorage.authToken, artist, $routeParams.shopId);

		shopPromise.then(function (data) {
			if(data.error) {
				$scope.artistError = data.error;
			} else {
				getShopFromUrl();
			}
		});
	}

	$scope.removeArtist = function (artist) {
		var shopPromise = shopService.removeArtist(sessionStorage.authToken, artist, $routeParams.shopId);

		shopPromise.then(function (data) {
			if(data.error) {
				$scope.artistError = data.error;
			} else {
				getShopFromUrl();
			}
		});
	}

	$scope.addAdmin = function (admin) {
		var shopPromise = shopService.addAdmin(sessionStorage.authToken, admin, $routeParams.shopId);

		shopPromise.then(function (data) {
			if(data.error) {
				$scope.adminError = data.error;
			} else {
				getShopFromUrl();
			}
		});
	}

	$scope.removeAdmin = function (admin) {
		var shopPromise = shopService.removeAdmin(sessionStorage.authToken, admin, $routeParams.shopId);

		shopPromise.then(function (data) {
			if(data.error) {
				$scope.adminError = data.error;
			} else {
				getShopFromUrl();
			}
		});
	}

	$scope.addReview = function() {
		var shopPromise = shopService.addReview(sessionStorage.authToken, $scope.review, $routeParams.shopId);

		shopPromise.then(function (data) {
			if(data) {
				$window.location.reload();
			} else {
				// Error handling.
			}
		});
	}


	$scope.redirect = function() {
		var shopPromise = shopService.isAdmin(sessionStorage.authToken, $routeParams.shopId);

		shopPromise.then(function (data) {
			if(!data || data.error) {
				$location.path("/shop/" + $routeParams.shopId);
			}
		});	
	}

	function isAdmin() {
		var shopPromise = shopService.isAdmin(sessionStorage.authToken, $routeParams.shopId);

		shopPromise.then(function (data) {
			if(data.error) {
				$scope.isAdmin = false;
			} else {
				$scope.isAdmin = data;
			}
		});
	}

	function getShopFromUrl() {
		var shopPromise = shopService.getDetails($routeParams.shopId);

		shopPromise.then(function (data) {
			if(data.error) {
				window.location = "/";
			}
			
			$scope.shopDetails = data;
			$rootScope.shop = data;

			$scope.glance = {};
			$scope.glance.knownFor = data.glance.knownFor;

			$scope.sunday = {};
			$scope.monday = {};
			$scope.tuesday = {};
			$scope.wednesday = {};
			$scope.thursday = {};
			$scope.friday = {};
			$scope.saturday = {};

			$scope.sunday.open = data.hours.sunday.isOpen ? data.hours.sunday.open : 'Closed';
			$scope.monday.open = data.hours.monday.isOpen ? data.hours.monday.open : 'Closed';
			$scope.tuesday.open = data.hours.tuesday.isOpen ? data.hours.tuesday.open : 'Closed';
			$scope.wednesday.open = data.hours.wednesday.isOpen ? data.hours.wednesday.open : 'Closed';
			$scope.thursday.open = data.hours.thursday.isOpen ? data.hours.thursday.open : 'Closed';
			$scope.friday.open = data.hours.friday.isOpen ? data.hours.friday.open : 'Closed';
			$scope.saturday.open = data.hours.saturday.isOpen ? data.hours.saturday.open : 'Closed';

			$scope.sunday.close = data.hours.sunday.close;
			$scope.monday.close = data.hours.monday.close;
			$scope.tuesday.close = data.hours.tuesday.close;
			$scope.wednesday.close = data.hours.wednesday.close;
			$scope.thursday.close = data.hours.thursday.close;
			$scope.friday.close = data.hours.friday.close;
			$scope.saturday.close = data.hours.saturday.close;

			var overall_list = _.map(data.reviews, function(results) {
				return results.overall;
			})

			var rating = 0;

			for (x in overall_list) {
				if(overall_list[x] == null) {
					continue;
				} else {
					rating += overall_list[x]
				}
			}

			rating /= overall_list.length;

			$scope.shop.rating = rating;
		});
	}
});

app.controller('headerController', function ($scope, $rootScope){
	
	$rootScope.$watch('shop', function() {
		$scope.url 		= apiURL;
		$scope.cdnPort 	= cdnPort;
		$scope.shop 	= $rootScope.shop;

		if($scope.shop) {
			$scope.isShop = true;
		} else {
			$scope.isShop = false;
		}
	});

	$scope.artistThumb = function(url) {
		return "url(http://" + $scope.url + ":" + $scope.cdnPort + "/gallery/" + url + ")";
	}

});

app.controller('staffController', function ($scope, $routeParams, $route, staffService) {
	$scope.url 		= apiURL;
	$scope.cdnPort 	= cdnPort;
	

	$scope.getUnauthorizedShops = function() {
		var promise = staffService.getUnauthorizedShops(sessionStorage.authToken);

		promise.then(function(data) {
			$scope.unautherizedShops = data;
		});
	};

	$scope.approveShop = function() {
		var promise = staffService.addComment(sessionStorage.authToken, $routeParams.shopID);
		
		promise.then(function (data) {
				if(data.error) {
					$scope.errorText = data.error;
					$scope.disable = false;
					$("html, body").animate({ scrollTop: "30px" });
				} else {
					$scope.disable = false;
					$scope.errorText = null;
					$scope.approvalResult = data.message;
					$location.path( "/artist/" + data.url);
					location.reload();
				}
			});
	}
});