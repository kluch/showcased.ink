var app = angular.module('sciApp');

app.directive("focusReview", function() {
	return {
		restrict: "A",
		link: function (scope, elem, attrs) {
			$(elem).click(function() {
				$("#newReview").focus();
				$("html, body").animate({ scrollTop: $(document).height() }, 500, "linear");
			})
		}
	}
});

// Manages header/masthead effect based on location.
app.directive("headerEffects", function () {
	return {
		restrict: "A",
		link: function(scope, elem, attrs) {

			scope.$watch(function () {
			    return location.hash
			}, function (value) {

				if(value.indexOf("shop") > 0) {
					$(document).scroll(function() {
						if ($(this).scrollTop() % 2 == 0 || $(this).scrollTop() < 450) {
							var vague = $("#masthead").Vague({intensity: $(this).scrollTop() / 100});
							vague.blur();
						}
					});
					
					$("#masthead-shop").show();
				}

				if(value.indexOf("success") > 0 || value.indexOf("create") > 0 || value.indexOf("change_picture") > 0 || value.indexOf("artist") > 0 || value.indexOf('profile') > 0) {
					$("#masthead-shop").hide();
					$("#masthead").css('display', 'none !important');
					scope.isShop = true;
				}

				if(value.indexOf("shop") == -1) {
					$("#masthead-shop").hide();
				}


				// If dashboard page.
				if ((value.indexOf("artist") > -1 || value.indexOf("profile") > -1 || value.indexOf("search") > -1 || value.indexOf("photo") > -1 
					|| value.indexOf("shop/create") > -1 || value.indexOf("shop/success") > -1 || value.indexOf("change_picture") > -1)) {
					// If artist
					$("#header").css("background", "url('/img/wallpaper_tattoo_blur2.png') bottom");
					$("#searchRow").removeAttr('style').css("position", "fixed");
					$("#header").css("position", "fixed");
					$("#masthead").hide();
					$("#masthead-well").hide();
					$("#contentPane").css("top", "75px");
					$("#searchRow").css("position", "0px");
					$("#masthead").css("height", "0px");

				} else {
					$("#masthead").show();
					$("#masthead-well").show();
					$("#searchRow").css("position", "absolute");
					$("#searchRow").css("top", "0px");
					$("#contentPane").css("top", "");
					$("#header").css("background", "");
					$("#masthead").css("height", "450px");
				}

				$(document).scroll(function() {
					if((value.indexOf("artist") == -1 && value.indexOf("profile") == -1 && value.indexOf("search") == -1 && value.indexOf("photo") == -1 && value.indexOf("shop/create") == -1 
						&& value.indexOf("shop/success") == -1 && value.indexOf("change_picture") == -1)) {
						$("#masthead").show();
						$("#contentPane").css("top", "");
						$("#header").css("background", "");
						$("#masthead").css("height", "450px");
						$("#header").css("box-shadow", "0px 500px rgba(0, 0, 0, 0) inset");


						if(value.indexOf("shop") == -1) {
							if ($(this).scrollTop() % 2 == 0 || $(this).scrollTop() < 450) {
								var vague = $("#masthead").Vague({intensity: $(this).scrollTop() / 100});
								vague.blur();
							}
						}

						$("#header").css("background", "rgba(0,0,0," + $(this).scrollTop() / 1000 + ")");

						if ($(this).scrollTop() > 310) {

							if(scope.shop) {
								$("#header").css("background", "url('http://"+ scope.url +":"+ scope.cdnPort + "/gallery/"+scope.shop.background+"') center");
								$("#header").css("background-size", "cover");
								$("#header").css("box-shadow", "0px 500px rgba(0, 0, 0, 0.4) inset");

							} else {
								$("#header").css("background", "url('/img/wallpaper_tattoo_blur2.png') bottom");
							}
							$("#small-title").show();
							$("#searchRow").css("position", "fixed");
							$("#searchRow").css("top", "75px");
						} else {
							$("#small-title").hide();
							$("#searchRow").css("position", "absolute");
							$("#searchRow").css("top", "0px");
						}

					} else if (value.indexOf("artist") > -1 || value.indexOf("profile") > -1 || value.indexOf("search") > -1 || value.indexOf("photo") > -1 || value.indexOf("shop/create") > -1 
						|| value.indexOf("shop/success") > -1 || value.indexOf("change_picture") > -1) {
						// If artist or profile
						$("#masthead").hide();
						$("#masthead").css("height", "0px");
						$("#header").css("background", "url('/img/wallpaper_tattoo_blur2.png') bottom");
						$("#searchRow").removeAttr('style').css("position", "fixed");
						$("#header").css("position", "fixed");
						$("#contentPane").css("top", "75px");
						$("#searchRow").css("position", "0px");
					}
				});
           });
		}
	}
});

app.directive("scrollToSearch", function () {
	return {
		restrict: 'A',
		link: function(scope, elem, attr) {
			$(elem).click(function() {
				$("html, body").animate({ scrollTop: 160 }, 600);
				$("#searchRow input").focus();
			})
		}
	}
});

app.directive("scrollToComments", function () {
	return {
		restrict: 'A',
		link: function(scope, elem, attr) {
			$(elem).click(function() {
				$("html, body").animate({ scrollTop: $(".comments").offset().top }, 600);
			})
		}
	}
});

// Hide uploaded images that are not found.
app.directive('imageExists', function() {
    return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != '../img/not-found.jpg') {
              attrs.$set('src', '../img/not-found.jpg');
            }
          });
        }
    }
});

// Tag Input
app.directive('tagInput', function() {
	return {
		restrict: 'E',
		require: 'ngModel',
		templateUrl: '../templates/widgets/taginput.html',
		scope: {
			placeholder: "@placeholder",
			model: '=ngModel'
		},
		link: function(scope, elem, attr) {
			scope.model = [];
			
			$(elem.find('input')).bind('keypress', function(e) {
				if(e.keyCode == 13) {

				 scope.$apply( function() {
				 	if(scope.model.indexOf(scope.input.tags) == -1) {
						scope.model.push(scope.input.tags);
					}

					scope.input.tags = '';
			      });

					e.preventDefault();
				}		
			});

		 	var availableTags = [
		      "Color",
		      "Black & White",
		      "Sleeve"
		    ];

		    $(elem.find('input')).autocomplete({
		      source: availableTags,
		      select: function(event, input) {
		      		scope.input.tags = input.item.label;
		      }
		    });

	    	scope.close = function(tag) {
				scope.model = _.without(scope.model, tag);
			}
		}
	}
});

app.directive('phoneInput', function() {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			$(element).mask('(000) 000-0000');
		}
	}
});

app.directive('dateInput', function() {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			$(element).mask('00/00/0000');
		}
	}
});

app.directive('zipInput', function() {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			$(element).mask('00000');
		}
	}
});

app.directive('timePicker', function() {
	return {
		restrict: 'C',
		link: function(scope, elem, attr) {
			$(elem).timepicker({
				noneOption: {
					label: 'Closed',
					value: 'Closed'
				}
			}).on('timeFormatError', function(event) {
				$(this).val('');
			}).on('changeTime', function(event) {
				switch (attr.ngModel) {
					case 'sunday.open':
						$("#sunday-close").timepicker('option', 'minTime', $(this).val());
						break;
					case 'sunday.close':
						$("#sunday-open").timepicker('option', 'maxTime', $(this).val());
						break;
					case 'monday.open':
						$("#monday-close").timepicker('option', 'minTime', $(this).val());
						break;
					case 'monday.close':
						$("#monday-open").timepicker('option', 'maxTime', $(this).val());
						break;
					case 'tuesday.open':
						$("#tuesday-close").timepicker('option', 'minTime', $(this).val());
						break;
					case 'tuesday.close':
						$("#tuesday-open").timepicker('option', 'maxTime', $(this).val());
						break;
					case 'wednesday.open':
						$("#wednesday-close").timepicker('option', 'minTime', $(this).val());
						break;
					case 'wednesday.close':
						$("#wednesday-open").timepicker('option', 'maxTime', $(this).val());
						break;
					case 'thursday.open':
						$("#thursday-close").timepicker('option', 'minTime', $(this).val());
						break;
					case 'thursday.close':
						$("#thursday-open").timepicker('option', 'maxTime', $(this).val());
						break;
					case 'friday.open':
						$("#friday-close").timepicker('option', 'minTime', $(this).val());
						break;
					case 'friday.close':
						$("#friday-open").timepicker('option', 'maxTime', $(this).val());
						break;
					case 'saturday.open':
						$("#saturday-close").timepicker('option', 'minTime', $(this).val());
						break;
					case 'saturday.close':
						$("#saturday-open").timepicker('option', 'maxTime', $(this).val());
						break;

				}
			});
		}
	}
});

app.directive('affixMenu', function() {
	return {
		restrict: 'C',
		link: function(scope, elem, attr) {
			$(elem).affix({
		  		offset: {
				    top: 400		  
				}
			})
		}
	}
})

app.directive('shareFacebook', function() {
	return {
		restrict: 'A',
		link: function(scope, elem, attr) {
			$(elem).click(function() {
				FB.ui({
					method: 'feed',
				  	link: 'http://' + scope.url + '/#/photo/' + scope.image._id,
			  		caption: "Find great local tattoo artists and shops rated and reviewed by enthusiasts like you on Showcased.Ink.",
			  		picture: 'http://' + scope.url + ":" + scope.cdnPort + "/gallery/" + scope.image.path,
			  		name: scope.image.title + " by " + scope.image.artist.fName + " " + scope.image.artist.lName 
				}, function(response){});
			})
		}
	}
})

app.directive('starRating', function() {
	return {
		restrict: 'A',
		templateUrl: '../templates/widgets/stars.html',
		scope:{
			ngModel: '='
		}
	}
})