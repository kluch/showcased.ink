#!/usr/bin/env bash

#update apt-get package list
sudo apt-get update

#install curl so nodejs can be installed
sudo apt-get install -y curl

#install nodejs
curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install -y nodejs
#update npm
#curl https://www.npmjs.org/install.sh | sudo sh

#install git and set the all important colors
sudo apt-get install -y git

#install python dependencies
sudo apt-get install -y build-essential
sudo apt-get install -y libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev

#install python for bcrypt compile
wget https://www.python.org/ftp/python/2.7.9/Python-2.7.9.tgz
tar -xvf Python-2.7.9.tgz
cd Python-2.7.9
./configure
make
sudo make install
cd ..

rm -f Python-2.7.9.tgz

#install mongo
curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-2.6.7.tgz
tar -zxvf mongodb-linux-x86_64-2.6.7.tgz
mkdir -p mongodb
mv -n mongodb-linux-x86_64-2.6.7/ mongodb/mongodb-2.6.7

#This uses a custom profile so that mongo is added to the $PATH
sudo cp -f /vagrant/vagrant/.profile /home/vagrant/.profile

rm -f mongodb-linux-x86_64-2.6.7.tgz

# The lines below probably need to be run manually after first install
# export PATH=~/mongodb/mongodb-2.6.7/bin:$PATH
# git config --global color.ui auto

#some stuff I haven't removed
#sudo apt-get install -y apache2
#if ! [ -L /var/www ]; then
#  rm -rf /var/www
#  ln -fs /vagrant /var/www
#fi